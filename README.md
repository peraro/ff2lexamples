Analytic results of [arXiv:1608.01902 \[hep-ph\]](http://arxiv.org/abs/1608.01902)
==================================================================================

This repository contains full analytic results for on-shell integrands
of the maximal cuts of the planar penta-box and the non-planar
double-pentagon topologies in Yang-Mills theory, for an independent
set of helicity configurations.

These results have been presented, as examples of multivariate
functional reconstruction using finite-field methods, in the following
paper

- T. Peraro, "*Scattering amplitudes over finite fields and
  multivariate functional reconstruction*,"
  [arXiv:1608.01902 \[hep-ph\]](http://arxiv.org/abs/1608.01902).

to which we refer to for further details.

Results for the penta-box and the double-pentagon topologies can be
found in the subdirectories `pentabox` and `doublepentagon` of the
repository respectively.  Each file in these subdirectories contains
the coefficients of the on-shell integrand corresponding to an
independent helicity configuration, which is specified by the file
name.

Each non-empty line of these files has the form

```
  c({a1,a2,...}, i) = ...
```

where on the r.h.s. of the equal sign there is an analytic expression
in the variables `x2`, `x3`, `x4`, `x5` defined in sect. 5.1 of the
paper.  In an on-shell integrand, this coefficient multiplies the
`i`-th power of `ds-2` and the integrand-basis element with
multi-index `{a1,a2,...}`, as defined in equations 5.14--5.17 of the
paper for the penta-box topology and in equations 5.19--5.22 for the
double-pentagon.  For brevity, we do not list the coefficients of
integrand-basis elements which vanish for all the powers `i` of
`ds-2`.
